import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class iDrinkPinScreen here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class iDrinkPinScreen extends Actor implements iDrinkScreens
{
    public void loadScreen(World world){
        world.addObject(this, 400, 300);
        for(int i =0; i<Button.xCoordinates.length; i++){
            for(int j=0; j<Button.yCoordinates.length; j++){
                world.addObject(new Button(),Button.xCoordinates[i],Button.yCoordinates[j]);
            }    
        }
    }
    
    /**
     * Act - do whatever the iDrinkPinScreen wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
          
    }    
}
