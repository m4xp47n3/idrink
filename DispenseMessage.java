import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class DispenseMessage here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class DispenseMessage extends Actor
{
    /**
     * Act - do whatever the DispenseMessage wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if(Greenfoot.mouseClicked(this))
        {
         //System.out.println("x= " + getX() + " y= " +  getY());          
         getWorld().removeObject(this);
        }
    }    
}
