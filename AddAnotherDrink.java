import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.awt.Color;
/**
 * Write a description of class AddAnotherDrink here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class AddAnotherDrink extends DrinkComponent
{
    
     int x=0,y=0;
    SelectDrink selectDrink = new SelectDrink();
    GreenfootImage test=getImage();
    Coke coke=new Coke();
    Fanta fanta=new Fanta();
    Gatorade gatorade=new Gatorade();
    MountainDew mountainDew=new MountainDew();
    Pepsi pepsi = new Pepsi();
    int drinkNumber=0;
    static int noOfDrinks=1;
            static int finalNumberOfDrinks=0;
          public  int drink=0;
    int[] maxDrinkValues={10,9,8,7,6};
    //int[] maxDrinkValues = getWorld().getObjects(ConcreteSubjectInventory.class);
    public AddAnotherDrink (){
//      GreenfootImage test = new GreenfootImage(25,25);
      // test=getImage();
    //When the object is initialised, there should be a parameter in the constructor which will give the n th block to write in 
        test.setFont(new java.awt.Font("Calibri",java.awt.Font.BOLD,12));
        test.drawString("0", 126, 13);
        setImage(test);
    }
    
    public void setMaxDrinksValues(){
        java.util.List objs = getWorld().getObjects(ConcreteSubjectInventory.class);
        maxDrinkValues = ((ConcreteSubjectInventory)objs.get(0)).getCount();
    }    
    /**
     * Act - do whatever the AddAnotherDrink wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
            int ComboDropDownStartX=getX()-2; //397
     int ComboDropDownStartY=getY()-20;    //180
     int ComboDropDownEndX=getX()+20;   //409
     int ComboDropDownEndY=getY()-7;    //196
     //MouseInfo mouse = Greenfoot.getMouseInfo();

     
     //Coke , Fanta, Pepsi, Mountain Dew, Gatorade
     
     
     if(Greenfoot.getMouseInfo()!=null){
        x= Greenfoot.getMouseInfo().getX();
           y= Greenfoot.getMouseInfo().getY();
          
              
           
           
           
           
           
           
           
        // Add your action code here.
        if(Greenfoot.mouseClicked(this)){
            if (x > ComboDropDownStartX && x < ComboDropDownEndX && y > ComboDropDownStartY && y < ComboDropDownEndY){
               System.out.println("Dropdown Clicked");
               try{
              getWorld().addObject(selectDrink,getX()-53,getY()+18 );}
              catch(Exception e){
                  
                }
            }
            else {
            getWorld().removeObject(selectDrink);
            }
            
            
            if (x > (getX()+44) && x <(getX() + 70) && y >(getY()-36) && y<(getY()-5)){
                System.out.println("Plus sign Working");
               // displayNoOfDrinks(10,1);
                /*********************************************/
                if(drink==1 || drink==0)
                displayNoOfDrinks(maxDrinkValues[0],1);
                 if(drink==2)
                displayNoOfDrinks(maxDrinkValues[1],1);
                else if(drink==3)
                displayNoOfDrinks(maxDrinkValues[2],1);
                else if(drink==4)
                displayNoOfDrinks(maxDrinkValues[3],1);
                else if(drink==5)
                displayNoOfDrinks(maxDrinkValues[4],1);
                /*********************************************/
            }
            
           else if (x > (getX()+76) && x <(getX() + 103) && y >(getY()-36) && y<(getY()-5)){
                System.out.println("Minus sign Working");
               // displayNoOfDrinks(10,0);
                 /*******************************************/
                //if(drink==1)
                //System.out.println("Current Drink :"+drink);
                if(drink==0 || drink==1)
                displayNoOfDrinks(maxDrinkValues[0],0);
                 if(drink==2)
                displayNoOfDrinks(maxDrinkValues[1],0);
                else if(drink==3)
                displayNoOfDrinks(maxDrinkValues[2],0);
                else if(drink==4)
                displayNoOfDrinks(maxDrinkValues[3],0);
                else if(drink==5)
                displayNoOfDrinks(maxDrinkValues[4],0);
                /*******************************************/
            }
            
            //296
            if ( x > (getX()-102) && x <  (getX()-86) && y > (getY()+7) && y < (getY()+23)){
                    System.out.println("Add another Drink");
                    if(noOfDrinks<5)
                    {noOfDrinks++;
                 //       selectDrink( drink, drinkNumber);
                 
                     addDrink();
                }
            }
            
            
            System.out.println("ComboStarting point x : "+getX()+"ComboStarting point y : "+ getY());
            System.out.println("Mouse Click point x : "+x+"Mouse Click point y : "+ y);
            System.out.println();
            
            
        }
        
      
      
        
        if(Greenfoot.mouseClicked(selectDrink)){
         //300 , 189    
         int Startx=getX()-120;
        int EndX=getX()-22;
        int StartY=getY()-13;
        int width=14;
        int b=(y-StartY)/width;
        System.out.println("The value of b is : "+b);
        //---GreenfootImage test1=getImage();

        switch(b){
          case 0: 
               
              
               {
                   getWorld().removeObject(selectDrink);
                   getWorld().removeObject(coke);
                 //---test1.drawImage(new GreenfootImage("C:/College/CMPE 202/lab3/CokeSelection.jpg"),126,13);
                   getWorld().addObject(coke,getX()-54,getY()-14);
                   drink=1;
                   System.out.println("Case 0 of Switch Case");
                   //getWorld().removeObject(selectDrink);
                   
                   }
                   break;
              case 1: 
               
              
               {
                   getWorld().removeObject(selectDrink);
                   getWorld().removeObject(fanta);
                   //---test.drawImage(new GreenfootImage("C:/College/CMPE 202/lab3/FantaSelection.jpg"),getX(),getY());
                   getWorld().addObject(fanta,getX()-54,getY()-14);
                   drink=2;
                   System.out.println("Case 1 of Switch Case");
                  // getWorld().removeObject(selectDrink);
                   }
                   break;   
                   case 2: 
               
             
               {
                   getWorld().removeObject(selectDrink);
                   getWorld().removeObject(pepsi);
                   //---test.drawImage(new GreenfootImage("C:/College/CMPE 202/lab3/PepsiSelection.jpg"),getX(),getY());
                   getWorld().addObject(pepsi,getX()-54,getY()-14);
                   drink=3;
                   System.out.println("Case 2 of Switch Case");
                   }
                   break;  
                   
                   case 3: 
               
         
               {
                   getWorld().removeObject(selectDrink);
                   getWorld().removeObject(mountainDew);
                   //---test.drawImage(new GreenfootImage("C:/College/CMPE 202/lab3/MountainDewSelection.jpg"),getX(),getY());
                   getWorld().addObject(mountainDew,getX()-54,getY()-14);
                   drink=4;
                   System.out.println("Case 3 of Switch Case");
                   }
                   break;   
                   
                   case 4: 
               
 
               {
                   getWorld().removeObject(selectDrink);
                   getWorld().removeObject(gatorade);
                   //---test.drawImage(new GreenfootImage("C:/College/CMPE 202/lab3/GatoradeSelection.jpg"),getX(),getY());
                   getWorld().addObject(gatorade,getX()-54,getY()-14);
                   drink=5;
                   System.out.println("Case 4 of Switch Case");
                   }
                   break; 
                   
                   
                   default:
                   getWorld().removeObject(selectDrink);
                   System.out.println("Default of Switch Case");
            }
        
        
        getWorld().removeObject(selectDrink);
        
        
        
        
        System.out.println("ComboStarting point x : "+ComboDropDownEndX+"ComboStarting point y : "+ ComboDropDownEndY);
            System.out.println("Mouse Click point x : "+x+"Mouse Click point y : "+ y);
            System.out.println();
        
        
        
                                 }
    
    
    
    
    
    
            }    
}


public void displayNoOfDrinks(int max, int add){
        
        
       // test.clear();
       //GreenfootImage test2=getImage();
       System.out.println("Current Drink :"+drink);
        test.setColor(new Color(255,255,255));
        test.setFont(new java.awt.Font("Calibri",java.awt.Font.BOLD,12));
        test.drawString(drinkNumber+"", 126, 13);
                test.setColor(new Color(0,0,0));
       // test.fillRect( 126, 13, 10, 10);
        if(add==1){
             if(drinkNumber<max)
                   { drinkNumber++;
                    changeNumber(1);
                    }
             
             String temp=drinkNumber+"";
           //  System.out.println("Drink number: "+drinkNumber);
             test.setFont(new java.awt.Font("Calibri",java.awt.Font.BOLD,12));
            
             test.drawString(drinkNumber+"", 126, 13);
             
             setImage(test);
             
             
          
            }
            else if(add==0){
                if(drinkNumber>0)   
                   { drinkNumber--;
                       changeNumber(0);
            }
                System.out.println("Drink number: "+drinkNumber);
                test.setFont(new java.awt.Font("Calibri",java.awt.Font.BOLD,12));
             test.drawString(drinkNumber+"", 126, 13);
                setImage(test);
            }
        }
               
    public void selectDrink(int drink1, int amt){
        ReserveDrinkClient.drinksSelected[drink1]+=amt;
    }
    
    public void addDrink(){
    getWorld().addObject(new AddAnotherDrink(),getX(),getY()+30);
    }
    public int[] getDrinks(){
        return ReserveDrinkClient.drinksSelected;
    }
    
    public static void changeNumber(int i){
        if(i==1)
        finalNumberOfDrinks++;
        if(i==0)
        finalNumberOfDrinks--;
        System.out.println("Current finalNumberOfDrinks : "+ finalNumberOfDrinks);
    }
    
    public static int getTotalDrinkNumber(){
    return finalNumberOfDrinks;
    }
}
