import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class TestSubject here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class TestSubject extends ConcreteSubjectInventory
{
    /**
     * Act - do whatever the TestSubject wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
   java.util.List<ConcreteSubjectInventory> a;
    public TestSubject(World world,String drinkName)
    {
        a=world.getObjects(ConcreteSubjectInventory.class);
        a.get(0).setDrinkName(drinkName);
        super.attach(new CokeObserver(a.get(0)));
        super.attach(new PepsiObserver(a.get(0)));
        super.attach(new FantaObserver(a.get(0)));
        super.attach(new DewObserver(a.get(0)));
        
    }
    public void incrementUpdate()
    {
       updateInventoryIncrement(); 
    }
     public void decrementUpdate()
    {
       updateInventoryDecrement(); 
    }
    public void act() 
    {
        // Add your action code here.
    }    
}
