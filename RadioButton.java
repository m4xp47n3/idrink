import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class RadioButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class RadioButton extends Actor
{
    /**
     * Act - do whatever the RadioButton wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
      GreenfootImage img;
    int x_loc;
    int y_loc;
    static mask objMask;
    
    public RadioButton()
    {
      img = new GreenfootImage(183,26);
      img.setColor( java.awt.Color.BLUE );
      img.fill();
        
      objMask = new mask();
      x_loc =0;
      y_loc= 0;
    //resetHighlight();
    }
    
    /*public void resetHighlight()
    {
        img.setTransparency(0);
        setImage(img);
    }*/
       
    public void act() 
    {
       if(Greenfoot.mouseClicked(this))
        {         
            System.out.println("x = " + x_loc + " y= " + y_loc);
            if(x_loc !=0 && y_loc!=0)
            {
                this.getWorld().removeObject(objMask);
            }
            
            //System.out.println("x= " + getX() + " y= " +  getY());          
         int x= getX();
         int y = getY();
        
         //objMask = new mask();
         this.getWorld().addObject(objMask,x, y);
         x_loc =x;
         y_loc = y;       
        }
    }    
    
    
}
