import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class DewObserver here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class DewObserver extends ObserverDrink
{
    /**
     * Act - do whatever the DewObserver wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public DewObserver(ConcreteSubjectInventory inventory)
    {
      super(inventory);
    }
    public void decrement()
    {
        if(this.inventory.getDrinkName().equalsIgnoreCase("Dew"))
        {
            this.inventory.decrementDew();
            System.out.println("Dew Count Decrements");
            System.out.println("Now the number of "+super.inventory.getDrinkName()+"in inventory: "+super.inventory.getDewCount());
        }
    }
        public void increment()
    {
        if(this.inventory.getDrinkName().equalsIgnoreCase("Dew"))
        {
            this.inventory.incrementDew();
            System.out.println("Dew Count Increments");
            System.out.println("Now the number of "+super.inventory.getDrinkName()+"in inventory: "+super.inventory.getDewCount());
        }
    }
    public void act() 
    {
        // Add your action code here.
    }    
}
