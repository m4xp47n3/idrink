import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Incrementer here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class CountDew extends Actor
{
    /**
     * Act - do whatever the Incrementer wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    int countD=0;
    
    public int getCountDew()
    {
        return countD;
    }
    public void setCountDew(int cnt)
    {
        this.countD=cnt;
    }
    
    public void act() 
    {
        // Add your action code here.
    }    
}
