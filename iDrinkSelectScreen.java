import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class iDrinkSelectScreen here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class iDrinkSelectScreen extends Actor implements iDrinkScreens 
{
static Actor tempMenuObj;   
 /**
     * Act - do whatever the iDrinkSelectScreen wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void loadScreen(World world)
    {
           //code start*************
       world.addObject(this, 400, 300);
        java.util.List objs = this.getObjectsInRange(250, null);
        if(objs != null)
            world.removeObjects(objs);    
        
                    Count countObj= new Count();
      CountPepsi countObjPepsi= new CountPepsi();
      CountFanta countObjFanta= new CountFanta();
      CountDew countObjDew= new CountDew();
      
      
        world.addObject(countObj,18,71);
        world.addObject(countObjPepsi,21,71);
        world.addObject(countObjFanta,24,71);
        world.addObject(countObjDew,27,71);
      
        
        ConcreteSubjectInventory inventory=new ConcreteSubjectInventory(world);
        world.addObject(inventory,20,20);
        
        //iDrinkSelectScreen pinScreen = new iDrinkSelectScreen();
        transPactor p= new transPactor(world);
             
        DisplaySign disp= new DisplaySign();
        DisplayPepsi dispPepsi= new DisplayPepsi();
        DisplayFanta dispFanta= new DisplayFanta();
        DisplayDew dispDew= new DisplayDew();
        
        
        ModLabel lbm = new ModLabel();
        lbm.setColor(java.awt.Color.WHITE);
       lbm.setDisplay(""+countObj.getCount());

       ModLabel lbmP = new ModLabel();
        lbmP.setColor(java.awt.Color.WHITE);
       lbmP.setDisplay(""+countObjPepsi.getCountPepsi());
       
       ModLabel lbmF = new ModLabel();
        lbmF.setColor(java.awt.Color.WHITE);
       lbmF.setDisplay(""+countObjFanta.getCountFanta());
       
            ModLabel lbmD = new ModLabel();
        lbmD.setColor(java.awt.Color.WHITE);
       lbmD.setDisplay(""+countObjDew.getCountDew());
      

       world.addObject(disp,579,263);
       world.addObject(lbm,576,268);
      
       world.addObject(dispFanta,579,295);
       world.addObject(lbmF,576,300);
       
       world.addObject(dispPepsi,579,324);
       world.addObject(lbmP,576,329);
       
       world.addObject(dispDew,579,349);
       world.addObject(lbmD,576,353);
       
       
        PlusPepsi plusPepsi= new PlusPepsi(world);
        MinusPepsi minusPepsi= new MinusPepsi(world);
        
        PlusSign plus= new PlusSign(world);
        MinusSign minus= new MinusSign(world);
       
        PlusFanta plusFanta= new PlusFanta(world);
        MinusFanta minusFanta= new MinusFanta(world);
        
        PlusDew plusDew= new PlusDew(world);
        MinusDew minusDew= new MinusDew(world);
       
  
       Label lbl= new Label();
       lbl.setColor(java.awt.Color.WHITE);
       lbl.setText("Drinks               Available");
      
       //lbl.setLocation(408,314);
        //dropButtonactor b= new dropButtonactor();
        
         
        world.addObject(lbl,425,307);
        world.addObject(p,403,175);
        
        world.addObject(plus,549,263);
        world.addObject(minus,609,263);
        
        world.addObject(plusFanta,549,295);
        world.addObject(minusFanta,609,295);
        
        world.addObject(plusPepsi,549,324);
        world.addObject(minusPepsi,609,324);
        
        world.addObject(plusDew,549,349);
        world.addObject(minusDew,609,349);
       
        
        System.out.println("Objects Added");
                    //code ends**************
    }
    public void act() 
    {
        // Add your action code here.
        //MouseInfo mouse = Greenfoot.getMouseInfo();
          int xChk,yChk;
      
         
         if(Greenfoot.mouseClicked(this))
            {
             xChk=Greenfoot.getMouseInfo().getX();
             yChk=Greenfoot.getMouseInfo().getY();
                  
                    // System.out.println("downarrow x="+xChk+" y ="+yChk);
                    
            }
    } 
    
}
