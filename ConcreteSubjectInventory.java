import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class ConcreteSubjectInventory here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ConcreteSubjectInventory extends Actor
{
    /**
     * Act - do whatever the ConcreteSubjectInventory wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    private String stateDrinkName;
    private int cokeCount;
    private int fantaCount=5;
    private int pepsiCount=5;
    private int dewCount=0;
    private  java.util.ArrayList<ObserverDrink> drinkObservers= new  java.util.ArrayList<ObserverDrink>();
    java.util.List<Count> count;
    
    public ConcreteSubjectInventory(){
    }    
     public ConcreteSubjectInventory(World world)
     {
        count=world.getObjects(Count.class);
         this.cokeCount=count.get(0).getCount();
         System.out.println("The count is "+this.cokeCount);
     }
    public void attach(ObserverDrink drink)
    {
        this.drinkObservers.add(drink);
    }
    public int getCokeCount()
    {
        return this.cokeCount;
    }
        public int getPepsiCount()
    {
        return this.pepsiCount;
    }
        public int getFantaCount()
    {
        return this.fantaCount;
    }
        public int getDewCount()
    {
        return this.dewCount;
    }
    
    public String getDrinkName()
    {
        return this.stateDrinkName;
    }
    public void setDrinkName(String drink)
    {
        this.stateDrinkName=drink;
    }
    public void updateInventoryIncrement()
    {
        System.out.println("The number of observers is "+drinkObservers.size());
        notifyIncrementDrinkObservers();
    }
    public void updateInventoryDecrement()
    {
        System.out.println("The number of observers is "+drinkObservers.size());
        notifyDecrementDrinkObservers();
    }
    public void notifyDecrementDrinkObservers()
    {
        for(int i=0;i<drinkObservers.size();i++)
        {
            drinkObservers.get(i).decrement();
        }
    }
    public void notifyIncrementDrinkObservers()
    {
        for(int i=0;i<drinkObservers.size();i++)
        {
            drinkObservers.get(i).increment();
        }
    }
    
    
    public void decrementCoke()
    {
        if(this.cokeCount>0)
        this.cokeCount--;
    }
       public void decrementFanta()
    {
        if(this.fantaCount>0)
        this.fantaCount--;
    }
       public void decrementPepsi()
    {
        if(this.pepsiCount>0)
        this.pepsiCount--;
    }
       public void decrementDew()
    {
        if(this.dewCount>0)
        this.dewCount--;
    }
    
    //increment code
    
      public void incrementCoke()
    {
        this.cokeCount++;
    }
       public void incrementFanta()
    {
        this.fantaCount++;
    }
       public void incrementPepsi()
    {
        this.pepsiCount++;
    }
       public void incrementDew()
    {
        this.dewCount++;
    }
    
    public void act() 
    {
        // Add your action code here.
    }
    
    public int[] getCount(){  
        int[] inventoryCount = new int[5]; 
        inventoryCount[0]= getCokeCount();
        inventoryCount[1]= getFantaCount();
        inventoryCount[2]= getPepsiCount();
        inventoryCount[3]= getDewCount();
        inventoryCount[4]= getCokeCount();
        return inventoryCount;
    }    
}
