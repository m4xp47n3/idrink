/**
 * Write a description of class DispenseCommand here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class DispenseCommand extends ICommand 
{
Receiver receiver = new Receiver();

     public DispenseCommand(Receiver receiverObj)
    { 
    receiver = receiverObj;
    }

  public void execute()
   {
            receiver.Action_Dispense();
    }
}
