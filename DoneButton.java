import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class DoneButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class DoneButton extends Actor
{
    /**
     * Act - do whatever the DoneButton wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
       if(Greenfoot.mouseClicked(this))
       {
       World world;
       world = getWorld();
       world.removeObjects(getWorld().getObjects(TextBox.class)); 
       world.removeObjects(getWorld().getObjects(CommentsMessage.class));
       world.removeObject(this);
       }
    }    
}
