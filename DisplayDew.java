import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class DisplaySign here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class DisplayDew extends Actor
{
    /**
     * Act - do whatever the DisplaySign wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    ModLabel lbl;
    public DisplayDew()
    {
        lbl= new ModLabel();
        
    }
    public ModLabel getLabel()
    {
        return this.lbl;
    }
    public void setLabel(ModLabel lbl)
    {
        this.lbl=lbl;
        System.out.println("The Label is now "+lbl.getText());
        //lbl.setColor(java.awt.Color.WHITE);
        displayLabel();
    }
    public void displayLabel()
    {
        this.getWorld().addObject(lbl,576,353);
    }
    public void act() 
    {
        // Add your action code here.
        //displayLabel();
    }    
}
