import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Menu here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Menu extends Actor
{
     GreenfootImage img;
    GreenfootImage img_black;
    public static String numbers[];
    public static final int[] xCoordinates = {401,403};
    public static final int[] yCoordinates = {204,234,264,402};
    Invoker invoker;
    Receiver receiver;
    CommentsCommand comment;
    DispenseCommand dispense;
    HelpCommand help;
    RateCommand rate;
    
    public Menu()
    {
      img = new GreenfootImage(183,26);
      img.setColor( java.awt.Color.BLUE );
      img.fill();

      numbers = new String[4];
      numbers[0]="Help";
      numbers[1]="Comments";
      numbers[2]="Rate";
      numbers[3]="Dispense";
      
      invoker = new Invoker();
      receiver = new Receiver();
      receiver.setMenu(this);
      
           
      resetHighlight();
    }
    
    
    public void act() 
    {         
       if(Greenfoot.mousePressed(this))
       {
       CommentsCommand comment = new CommentsCommand(receiver);
       DispenseCommand dispense = new DispenseCommand(receiver);
       HelpCommand help = new HelpCommand(receiver);
       RateCommand rate = new RateCommand(receiver);
       
           
            String input = getInput();
            //System.out.println("x= " + getX() + " y= " +  getY());            
            if(input =="Dispense")
            {
            img = new GreenfootImage(183,26);
            img.setColor( java.awt.Color.BLACK );
            img.fill();
            }
            else
            {
            img = new GreenfootImage(183,26);
            img.setColor( java.awt.Color.BLUE );
            img.fill();
            }
            
            highLight();
            Greenfoot.delay(10);
                     
            if(input.compareToIgnoreCase("Help")==0)
            {
             invoker.setCommand(help); 
            }
            else if(input.compareToIgnoreCase("Comments")==0)
            {
              invoker.setCommand(comment); 
            }
            else if(input.compareToIgnoreCase("Rate")==0)
            {
             invoker.setCommand(rate); 
            }
            else if(input.compareToIgnoreCase("Dispense")==0)
            {
              invoker.setCommand(dispense); 
            }
              
            invoker.executeCommand();
            resetHighlight(); 
            }   
    }  
    
    public void resetHighlight(){
        img.setTransparency(0);
        setImage(img);
       }
    
    
    public void highLight(){
        img.setTransparency(100);
        setImage(img);        
    } 
    
    public String getInput()
    {
        for(int i=0; i< yCoordinates.length; i++){
            for(int j=0; j< xCoordinates.length; j++){
                if(getX()==xCoordinates[j] && getY()==yCoordinates[i])
                {
                return numbers[i]; 
                }    
            }
        }    
        return "";
        
    }
}
