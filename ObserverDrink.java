import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class ObserverDrink here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ObserverDrink extends Actor
{
    /**
     * Act - do whatever the ObserverDrink wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    
    protected String drinkNameObserverState;
    protected ConcreteSubjectInventory inventory;
    
    public ObserverDrink(ConcreteSubjectInventory inventory)
    {
        this.inventory=inventory;
    }
    public void decrement()
    {
        
    }
     public void increment()
    {
        
    }
    public void act() 
    {
        // Add your action code here.
    }    
}
