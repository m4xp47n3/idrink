import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Button here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Button extends Actor
{
    public static final int[] xCoordinates = {332,403,475};
    public static final int[] yCoordinates = {297,332,370,405};
    public static int numbers[][];
    private GreenfootImage img;
    PinInspector insp = new PinInspector();
    PassMask iPin = new PassMask();
    static iPin pin = new iPin();
    
    //{(333,297),(405,297),(475,297),(333,333),(405,333),(475,333),(333,370),(405,370),(475,370)};
    
    public Button(){
        img = new GreenfootImage(72,37);
        img.setColor( java.awt.Color.BLUE );
        img.fill();
        
        initializeButton();
        
        numbers = new int[4][3];
        numbers[0][0] = 1;
        numbers[0][1] = 2;
        numbers[0][2] = 3;
        numbers[1][0] = 4;
        numbers[1][1] = 5;
        numbers[1][2] = 6;
        numbers[2][0] = 7;
        numbers[2][1] = 8;
        numbers[2][2] = 9;
        numbers[3][0] = 99;
        numbers[3][1] = 0;
        numbers[3][2] = 100;
        
        
     /*   pm= new PassMask();
        pm1= new PassMask();
        pm2= new PassMask();
        pm3= new PassMask();
    */
}    
    /**
     * Act - do whatever the One wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {  
        if(Greenfoot.mousePressed(this)){
            highLightButton();
            Greenfoot.delay(10);
            initializeButton();
            int tempio = getInput();
          //  iPin.passCode(tempio,this.getWorld());
          iPin.setWorld(this.getWorld());
            System.out.println(" Button clicked: "+tempio);
            
        
            pin.push(Integer.toString(tempio));
        }   
    }
    
    public void initializeButton(){
        img.setTransparency(0);
        setImage(img);
    }
    
    public void highLightButton(){
        img.setTransparency(100);
        setImage(img);
    } 
    
    public int getInput(){
        for(int i=0; i< yCoordinates.length; i++){
            for(int j=0; j< xCoordinates.length; j++){
                if(getX()==xCoordinates[j] && getY()==yCoordinates[i]){
                    return numbers[i][j];
                    //System.out.println(numbers[i][j]);
                    //break;
                }    
            }
        }    
        return 0;
    } 
    
  /*  public void passCode(int key){

 iDrinkWorld wd=(iDrinkWorld) this.getWorld();
                                
if(key > -1 && key < 10 ){
if(counter == 0)
wd.addObject(pm, 332, 212);
if(counter == 1)
wd.addObject(pm1, 378, 212);
 if(counter == 2)
 wd.addObject(pm2, 427, 212);
 if(counter == 3)
     wd.addObject(pm3, 475, 212);
 
     counter++;
}

                 if(key == 100)
                  {   
                      counter--;
                      
                      if(counter == 0)
                        wd.removeObject(pm);
                      if(counter == 1)
                         wd.removeObject(pm1);
                      if(counter == 2)
                         wd.removeObject(pm2);
                      if(counter == 3)
                         wd.removeObject(pm3);
                 
                      
                    }


}*/
}
