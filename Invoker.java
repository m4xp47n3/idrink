import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;
/**
 * Write a description of class Invoker here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Invoker 
{
    private ICommand command; 
    
    public void setCommand(ICommand commandObj)
    {
        command = commandObj;
    }
    
    public void executeCommand()
    {   
        command.execute();
    }
    
}

