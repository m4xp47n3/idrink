import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class transPactor here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class transPactor extends Actor
{
    /**
     * Act - do whatever the transPactor wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
        GreenfootImage p;
        //TestSubject test;
        java.util.List<ConcreteSubjectInventory> a;
    public transPactor(World world)
    {
        p = new GreenfootImage(187,28);
        a=world.getObjects(ConcreteSubjectInventory.class);
        //test= new TestSubject(this.getWorld());
        p.setColor(java.awt.Color.BLACK);
        p.fill();
        initialButton();
        System.out.println("The coke count is "+a.get(0).getCokeCount());
    }
    
    public void initialButton(){
        p.setTransparency(0);
        setImage(p);
    }
    public void onClick()
    {
       p.setTransparency(100);
       setImage(p);
    }
 
    public void displayMenuTemplate()
    {
       //this line adds the menu template
        this.getWorld().addObject(new menuDrinksactor(),402,305);
        //now setting the labels 
        Label lbl = new Label();
        lbl.setText("Coke                              "+a.get(0).getCokeCount());
        this.getWorld().addObject(lbl,402,354);
        Label lbl1 = new Label();
        lbl1.setText("Fanta                             "+a.get(0).getFantaCount());
        this.getWorld().addObject(lbl1,402,381);
            Label lbl2 = new Label();
        lbl2.setText("Pepsi                             "+a.get(0).getPepsiCount());
        this.getWorld().addObject(lbl2,402,404);
        
            Label lbl3 = new Label();
        lbl3.setText("Mountain Dew              "+a.get(0).getDewCount());
        this.getWorld().addObject(lbl3,402,434);
        
        //reading the files
        //reading ends
        /*try
        {
        //System.out.println("The output from file is "+readText());
        }
        catch(java.io.IOException e)
        {
           System.out.print(""+e); 
        }*/
    
    }   
    public void act() 
    {
        if(Greenfoot.mousePressed(this))
        {
            onClick();
            Greenfoot.delay(10);
            initialButton();
            displayMenuTemplate();
            
        }    
        // Add your action code here.
    }    
}
