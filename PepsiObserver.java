import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class PepsiObserver here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class PepsiObserver extends ObserverDrink
{
    /**
     * Act - do whatever the PepsiObserver wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public PepsiObserver(ConcreteSubjectInventory inventory)
    {
      super(inventory);
    }
    public void decrement()
    {
        if(this.inventory.getDrinkName().equalsIgnoreCase("Pepsi"))
        {
            this.inventory.decrementPepsi();
            System.out.println("Pepsi Count Decrements");
            System.out.println("Now the number of "+super.inventory.getDrinkName()+"in inventory: "+super.inventory.getPepsiCount());
        }
    }
     public void increment()
    {
        if(this.inventory.getDrinkName().equalsIgnoreCase("Pepsi"))
        {
            this.inventory.incrementPepsi();
            System.out.println("Pepsi Count Increments");
            System.out.println("Now the number of "+super.inventory.getDrinkName()+"in inventory: "+super.inventory.getPepsiCount());
        }
    }
    public void act() 
    {
        // Add your action code here.
    }    
}
