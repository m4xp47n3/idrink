import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class OkButtonHelp here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class OkButtonHelp extends Actor
{
    /**
     * Act - do whatever the OkButtonHelp wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if(Greenfoot.mouseClicked(this))
       {
       World world;
       world = getWorld();
       world.removeObjects(getWorld().getObjects(HelpMessage.class)); 
       world.removeObject(this);
       }
    }    
}
