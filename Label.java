import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Label here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Label extends Actor
{

    /**
     * Act - do whatever the Label wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    GreenfootImage img;
    //GreenfootImage display;
    String text;
public Label()
{
   img =  new GreenfootImage(200,200);
   //display= new GreenfootImage(27,19);
}

public String getText()
{
return this.text;
}    

  public void setText(String text){
img.clear();
img.drawString(text, 10,20);
setImage(img);
}

public void setColor(java.awt.Color c)
{
img.setColor(c);
setImage(img);
}
    public void act() 
    {
        // Add your action code here.
    }    
}
