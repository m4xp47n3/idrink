import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class iDrinkWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class iDrinkWorld extends World
{

    /**
     * Constructor for objects of class iDrinkWorld.
     * 
     */
    public iDrinkWorld()
    {    
        // Create a new world with 800x600 cells with a cell size of 1x1 pixels.
        super(800, 600, 1); 
        prepareWorld();
    }
    
    /**
     * Prepare the world for the start of the program. That is: create the initial
     * objects and add them to the world.
     */
    public void prepareWorld(){
        addObject(AppController.getInstance(), 0, 0);
        AppController.getInstance().launchPinScreen();
        //addObject(new MouseInfo(), 20, 20);
    }    
}
