import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class iDrinkPayment here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class iDrinkPaymentScreen extends Actor implements iDrinkScreens
{   World iWorld;
    public void loadScreen(World world){
        world.addObject(this, 400, 300);
        java.util.List objs = this.getObjectsInRange(250, null);
        if(objs != null)
            world.removeObjects(objs);
        int quantity = AddAnotherDrink.getTotalDrinkNumber();
        java.awt.Color color = (new iDrinkPaymentScreen()).getImage().getColorAt(220,198);
        Amount amt= new Amount(""+quantity*2 , color);
       this.getWorld().addObject(amt, 400, 200);
    }
    
    /**
     * Act - do whatever the iDrinkPayment wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.

        if(Greenfoot.mousePressed(this)){
            
            //MouseInfo mouse =  Greenfoot.getMouseInfo(); 
            
            if(Greenfoot.getMouseInfo().getX() > 308 && Greenfoot.getMouseInfo().getX() <495){
                 if( Greenfoot.getMouseInfo().getY()>388 && Greenfoot.getMouseInfo().getY() <415)
                  //getWorld().addObject(new iDrinkSplashScreen(), 400, 300);
                    iWorld = getWorld();
                    iWorld.addObject(new iDispenseScreen(), 400, 300);     
                    iWorld.addObject(new Menu(),403,204);
                    iWorld.addObject(new Menu(),403,234);
                    iWorld.addObject(new Menu(),403,264);
                    iWorld.addObject(new Menu(),401,402);
             
            }
        
        
        }
        
        
    }    
}
