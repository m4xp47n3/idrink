import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class iDrinkSplashScreen here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class iDrinkSplashScreen extends Actor implements iDrinkScreens
{
    public void loadScreen(World world){
        world.addObject(this, 400, 300);
        java.util.List objs = this.getObjectsInRange(5, null);
        if(objs != null)
            world.removeObjects(objs);
    }
    
    /**
     * Act - do whatever the iDrinkSplashScreen wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        // Add your action code here.
    }    
}
