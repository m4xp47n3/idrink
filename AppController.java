import greenfoot.*;
/**
 * Write a description of class AppController here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class AppController extends Actor implements iMenu
{
    // instance variables
    private static AppController appController;
    
    //Screens
    private iDrinkPinScreen iDrinkPinScreenObj = new  iDrinkPinScreen();
    private iDrinkSplashScreen iDrinkSplashScreenObj = new  iDrinkSplashScreen();
    private iDrinkSelectScreen iDrinkSelectScreenObj = new  iDrinkSelectScreen();
    private ReserveDrinkClient iDrinkReserveScreenObj = new  ReserveDrinkClient();
    private iDrinkPaymentScreen iDrinkPaymentScreenObj = new  iDrinkPaymentScreen();
    
    // Commands
    private iMenuCommand menuCommand1 = new iMenuCommand(iDrinkSelectScreenObj); 
    private iMenuCommand menuCommand2 = new iMenuCommand(iDrinkReserveScreenObj); 
    private iMenuCommand menuCommand3 = new iMenuCommand(iDrinkPaymentScreenObj); 
    
    

    /**
     * Constructor for objects of class AppController
     */
    private AppController()
    {
    }
    
    public static AppController getInstance(){
       if(null == appController){
           appController = new AppController();
       }
       return appController;
    }
    
    public void act(){
       
        if(null != Greenfoot.getMouseInfo()){
             int mx = Greenfoot.getMouseInfo().getX();
             int my = Greenfoot.getMouseInfo().getY();
                
            if(Greenfoot.mousePressed(iDrinkSelectScreenObj) && menu2Touched(mx, my)){
                menuItem2();
            } else if(Greenfoot.mousePressed(iDrinkSelectScreenObj) && menu3Touched(mx, my)){
                menuItem3();
            } else if(Greenfoot.mousePressed(iDrinkReserveScreenObj) && menu1Touched(mx, my)){
                menuItem1();
            } else if(Greenfoot.mousePressed(iDrinkReserveScreenObj) && menu3Touched(mx, my)){
                menuItem2();
            } else if(Greenfoot.mousePressed(iDrinkPaymentScreenObj) && menu1Touched(mx, my)){
                menuItem1();
            } else if(Greenfoot.mousePressed(iDrinkPaymentScreenObj) && menu2Touched(mx, my)){
                menuItem2();
            }
        }
    }  
        
    public void startApp(){
        iDrinkSplashScreenObj.loadScreen(this.getWorld());
        Greenfoot.delay(200);
        iDrinkSelectScreenObj.loadScreen(this.getWorld());
    }
    
    public void menuItem1(){
        menuCommand1.executeCommand(this.getWorld());
    }    
    
    public void menuItem2(){
        menuCommand2.executeCommand(this.getWorld());
    }
    
    public void menuItem3(){
        menuCommand3.executeCommand(this.getWorld());
    }
    
    public void launchPinScreen(){
        iDrinkPinScreenObj.loadScreen(this.getWorld());
    }    
    
    private boolean menu1Touched(int mx, int my){
        return ((mx > 293 && mx < 363) && (my > 115 && my < 145));    
    }    
    
    private boolean menu2Touched(int mx, int my){
        return ((mx > 364 && mx < 438) && (my > 115 && my < 145));
    }
    
    private boolean menu3Touched(int mx, int my){
        return ((mx > 439 && mx < 508) && (my > 115 && my < 145));
    }
}
