import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Label here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ModLabel extends Actor
{

    /**
     * Act - do whatever the Label wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
   
    GreenfootImage display;
    String text;
public ModLabel()
{
   display= new GreenfootImage(27,19);
}

public String getText()
{
return this.text;
}    
public void setDisplay(String disp)
{
 display.clear();
 display.drawString(disp,13,9);
 setImage(display);
 this.text=disp;
}

public void setColor(java.awt.Color c)
{
display.setColor(c);
setImage(display);
}
    public void act() 
    {
        // Add your action code here.
    }    
}
