import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class MouseInfo here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MouseInfo extends Actor
{
    /**
     * Act - do whatever the MouseInfo wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {  
        if(Greenfoot.getMouseInfo()!=null){  
           int mx = Greenfoot.getMouseInfo().getX();  
           int my = Greenfoot.getMouseInfo().getY();  
           System.out.println(mx+" "+my);
        }
        
    }    
}
