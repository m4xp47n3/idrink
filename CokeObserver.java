import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class CokeObserver here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class CokeObserver extends ObserverDrink
{
    /**
     * Act - do whatever the CokeObserver wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public CokeObserver(ConcreteSubjectInventory inventory)
    {
      super(inventory);
      
    }
    public void decrement()
    {
        if(this.inventory.getDrinkName().equalsIgnoreCase("Coke"))
        {
            this.inventory.decrementCoke();
            System.out.println("Coke Count Decrements");
            System.out.println("Number of"+super.inventory.getDrinkName()+"in inventory: "+super.inventory.getCokeCount());
        }
    }
    public void increment()
    {
        if(this.inventory.getDrinkName().equalsIgnoreCase("Coke"))
        {
            this.inventory.incrementCoke();
            System.out.println("Coke Count Increments");
            System.out.println("Number of"+super.inventory.getDrinkName()+"in inventory: "+super.inventory.getCokeCount());
        }
    }
    public void act() 
    {
        // Add your action code here.
    }    
}
