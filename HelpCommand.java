/**
 * Write a description of class HelpCommand here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class HelpCommand extends ICommand 
{
Receiver receiver;

    public HelpCommand(Receiver receiverObj)
    { 
    receiver = receiverObj;
    }

   public void execute()
   {
     receiver.Action_Help();
   }  
}
