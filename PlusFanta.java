import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Plus here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class PlusFanta extends Actor
{
    /*
     * Act - do whatever the PlusSign wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    ModLabel lbl;
    CountFanta countObj;
    java.util.List<CountFanta> countArr;
    java.util.List<DisplayFanta> dispObj;
    java.util.List<ModLabel> lblList;
    World world;
    
    public PlusFanta(World world)
    {
        //mask= new GreenfootImage(27,19);
        //mask.setColor(java.awt.Color.GRAY);
        //mask.fill();
        //initialButton();
        //countObj= new Count();
        countArr=world.getObjects(CountFanta.class);
        countObj=countArr.get(0);
        dispObj=world.getObjects(DisplayFanta.class);
        lbl=dispObj.get(0).getLabel();
        this.world=world;
        lblList=world.getObjects(ModLabel.class);
        
    }
    public void initialButton()
    {
        //mask.setTransparency(100);
        //setImage(mask);
    }
     public void onClick()
    {
       //mask.setTransparency(0);
       ///setImage(mask);
    }
    public void updateDisplayText()
    {
        
        lbl.setColor(java.awt.Color.WHITE);
        lbl.setDisplay(""+countObj.getCountFanta());
      //  System.out.println("The count is now "+countObj.getCount())
        //this.getWorld().addObject(lbl,664,331);
        
        dispObj.get(0).setLabel(lbl);
    }
  
    
    public void act() 
    {
       // updateDisplayText();
         if(Greenfoot.mousePressed(this))
        {
            //onClick();
            //Greenfoot.delay(10);
            //initialButton();
            //removing object modlabel 
            world.removeObject(lblList.get(1));
            int temp=countObj.getCountFanta()+1;
            countObj.setCountFanta(temp);
            updateDisplayText();
            TestSubject testSubj= new TestSubject(world,"Fanta");
            testSubj.incrementUpdate();
        } 
    }    
}
