import greenfoot.*;
/**
 * Write a description of class iMenuCommand here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class iMenuCommand implements Command
{
    iDrinkScreens screen;
    
    public iMenuCommand(iDrinkScreens screen){
        this.screen = screen;
    }
    
    public void executeCommand(World world){
        screen.loadScreen(world);
    }    
}
