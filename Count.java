import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Incrementer here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Count extends Actor
{
    /**
     * Act - do whatever the Incrementer wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    int count=10;
    
    public int getCount()
    {
        return count;
    }
    public void setCount(int cnt)
    {
        this.count=cnt;
    }
    
    public void act() 
    {
        // Add your action code here.
    }    
}
