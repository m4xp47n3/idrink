import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class MinusSign here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MinusPepsi extends Actor
{
    ModLabel lbl;
    CountPepsi countObj;
    java.util.List<CountPepsi> countArr;
    java.util.List<DisplayPepsi> dispObj;
    java.util.List<ModLabel> lblList;
    World world;
    public MinusPepsi(World world)
    {
        //mask= new GreenfootImage(27,19)
        //mask.setColor(java.awt.Color.GRAY);
        //mask.fill();
        //initialButton();
        //countObj= new Count();
        countArr=world.getObjects(CountPepsi.class);
        countObj=countArr.get(0);
        dispObj=world.getObjects(DisplayPepsi.class);
        lbl=dispObj.get(0).getLabel();
        this.world=world;
        lblList=world.getObjects(ModLabel.class);
        //System.out.println("Minus Object Initialised");
    }
    public void initialButton()
    {
        //mask.setTransparency(100);
        //setImage(mask);
    }
     public void onClick()
    {
       //mask.setTransparency(0);
       ///setImage(mask);
    }
    public void updateDisplayText()
    {
        lbl.setColor(java.awt.Color.WHITE);
           lbl.setDisplay(""+countObj.getCountPepsi());
      //  System.out.println("The count is now "+countObj.getCount());
        
        //this.getWorld().addObject(lbl,664,331);
        
        dispObj.get(0).setLabel(lbl);
    }
    
    public void act() 
    {
        //updateDisplayText();
         if(Greenfoot.mousePressed(this))
        {
            //onClick();
            //Greenfoot.delay(10);
            //initialButton();
            //System.out.println(" Minus Clicked ");
            int temp=0;
            world.removeObject(lblList.get(2));
            if(countObj.getCountPepsi()>0)
            temp=countObj.getCountPepsi()-1;
            countObj.setCountPepsi(temp);
            updateDisplayText();
             TestSubject testSubj= new TestSubject(world,"Pepsi");
            testSubj.decrementUpdate();
            
        } 
    }   
}
