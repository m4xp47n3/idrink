import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class FantaObserver here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class FantaObserver extends ObserverDrink
{
    /**
     * Act - do whatever the FantaObserver wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public FantaObserver(ConcreteSubjectInventory inventory)
    {
      super(inventory);
    }
    public void decrement()
    {
        if(this.inventory.getDrinkName().equalsIgnoreCase("Fanta"))
        {
            this.inventory.decrementFanta();
            System.out.println("Fanta Count Decrements");
            System.out.println("Now the number of "+super.inventory.getDrinkName()+"in inventory: "+super.inventory.getFantaCount());
        }
    }
        public void increment()
    {
        if(this.inventory.getDrinkName().equalsIgnoreCase("Fanta"))
        {
            this.inventory.incrementFanta();
            System.out.println("Fanta Count Increments");
            System.out.println("Now the number of "+super.inventory.getDrinkName()+"in inventory: "+super.inventory.getFantaCount());
        }
    }
    public void act() 
    {
        // Add your action code here.
    }    
}