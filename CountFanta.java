import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Incrementer here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class CountFanta extends Actor
{
    /**
     * Act - do whatever the Incrementer wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    int countF=5;
    
    public int getCountFanta()
    {
        return countF;
    }
    public void setCountFanta(int cnt)
    {
        this.countF=cnt;
    }
    
    public void act() 
    {
        // Add your action code here.
    }    
}
